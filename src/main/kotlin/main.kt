import kotlinx.browser.document
import kotlinx.browser.window
import kotlinx.html.*
import kotlinx.html.dom.append
import kotlinx.html.dom.create
import kotlinx.html.js.div
import kotlinx.html.js.onClickFunction
import org.w3c.dom.HTMLInputElement

fun main() {
    window.onload = {
        document.head!!.append.apply {
            title {
                +"Shortless"
            }

            link {
                href = "./favicon.apng"
                rel = "icon"
            }

            link {
                href = "https://use.fontawesome.com/releases/v5.8.1/css/all.css"
                rel = "stylesheet"
            }
            link {
                href = "https://fonts.googleapis.com/css?family=Fira+Sans:300,400,600&display=swap"
                rel = "stylesheet"
            }
            link {
                href = "./css/shortener.css"
                rel = "stylesheet"
            }
        }

        document.body!!.append.apply {
            div("main-block") {
                input {
                    id = "shorten-input"
                    classes = setOf("url-input")
                    type = InputType.url
                    placeholder = "Shorten your link"
                }
                button {
                    id = "shorten-button"
                    classes = setOf("url-button")
                    type = ButtonType.submit

                    onClickFunction = {
                        (document.getElementById("shorten-input") as HTMLInputElement).apply {
                            value = "TODO"
                            setAttribute("readonly", "true")
                        }

                        document.getElementById("shorten-button")!!.replaceWith(document.create.button {
                            id = "shorten-button"
                            classes = setOf("copy-button")
                            type = ButtonType.submit

                            onClickFunction = {
                                (document.getElementById("shorten-input") as HTMLInputElement).apply {
                                    select()
                                    setSelectionRange(0, 99999)
                                }
                                document.execCommand("copy")
                            }

                            span {
                                id = "shorten-button-text"
                                +"Copy"
                            }
                        })
                    }

                    span {
                        id = "shorten-button-text"
                        +"Go!"
                    }
                }
            }
            div("footer-block") {
                span("footer-text") {
                    +"Shortless"
                }
                span("footer-muted-text") {
                    +"Powered by Kotless and Ktor"
                }
            }
        }
    }
}
