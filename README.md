# Url shortener Demo

Based on Kotless Example (https://github.com/JetBrains/kotless/tree/master/examples/ktor/shortener)


Build: `./gradlew browserDistribution` (output in `build/distributions`)

Run: `./gradlew browserRun`
